# frozen_string_literal: true

class Booking < ApplicationRecord
  scope :bookings_of_day, lambda { |day, cut|
                            where('start >= ? and end >= ? and end <= ?',
                                  day.beginning_of_day, cut, day.next_day.beginning_of_day)
                          }
  scope :bookings_in_range, ->(s, e) { where('(start >= ? and start < ?) or (end > ? and end <= ?)', s, e, s, e) }
  default_scope { order(start: :asc) }

  def self.for_day(day, start_min, end_min)
    b = Booking.new
    b.start = Time.utc(day.year, day.month, day.day, start_min / 60, start_min % 60, 0)
    b.end = b.start.advance(minutes: (end_min - start_min))
    b
  end

  def expired?
    start < Time.now.utc
  end
end
