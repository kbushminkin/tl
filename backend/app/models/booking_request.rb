# frozen_string_literal: true

class BookingRequest
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  attr_reader :day, :from, :to

  validates :day, :from, :to, presence: true
  validates :from, :to, numericality: { only_integer: true }
  validates :from, comparison: { less_than: :to }
  validate :no_more_then_day_to_book
  validate :from_increment_validate
  validate :to_increment_validate

  def initialize(day, from, to)
    @from = from
    @to = to
    @day = day
  end

  private

  def no_more_then_day_to_book
    return if to.blank?

    return unless to.to_i > AvailabilityPlanner::MINS_IN_DAY

    errors.add(:to, 'Booking should not be over day')
  end

  def from_increment_validate
    validate_increment_for_field(:from)
  end

  def to_increment_validate
    validate_increment_for_field(:to)
  end

  def validate_increment_for_field(field)
    return if send(field).blank?

    return unless send(field).to_i % AvailabilityPlanner::TIME_INC != 0

    errors.add(field, "#{field} should be increment of #{AvailabilityPlanner::TIME_INC}")
  end
end
