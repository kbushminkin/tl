# frozen_string_literal: true

class AvailabilityPlanner
  TIME_INC = 15
  MINS_IN_DAY = 1440
  def self.get_minutes(time)
    time.hour * 60 + time.min
  end

  def self.get_slots(duration, booked_slots, now, day)
    available_slots = []
    left = 0
    if day == now.to_date
      mins = get_minutes(now) + (now.sec.positive? ? 1 : 0)
      left = (mins / TIME_INC) * TIME_INC + ((mins % TIME_INC).zero? ? 0 : TIME_INC)
    end
    booked_slots.each do |bs|
      start_time = get_minutes(bs.start)
      (left..(start_time - duration)).step(TIME_INC) do |n|
        available_slots.append(n)
      end
      left = (bs.end.day - bs.start.day) * MINS_IN_DAY + get_minutes(bs.end)
    end

    (left..(MINS_IN_DAY - duration)).step(TIME_INC) do |n|
      available_slots.append(n)
    end

    available_slots
  end
end
