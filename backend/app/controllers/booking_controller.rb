# frozen_string_literal: true

class BookingController < ApplicationController
  before_action :set_no_cache_headers, only: [:availability]

  def availability
    day = params[:day].to_date
    duration = params[:duration].to_i

    now = Time.now.utc

    render json: [].to_json and return if day < now.to_date

    booked_slots = Booking.bookings_of_day(day, now).all

    available_slots = AvailabilityPlanner.get_slots(duration, booked_slots, now, day)

    render json: available_slots.to_json
  rescue ArgumentError
    render json: { error: 'Date or duration parameters are invalid' }, status: :unprocessable_entity
  end

  def book
    attrs = params.require(:booking_request).permit(%i[day from to])

    br = BookingRequest.new(attrs[:day], attrs[:from], attrs[:to])
    render json: { errors: br.errors }, status: :unprocessable_entity and return unless br.valid?

    day = attrs[:day].to_date
    from = attrs[:from].to_i
    to = attrs[:to].to_i

    booking = Booking.for_day(day, from, to)

    render json: { error: "Can't book for past" }, status: :gone and return if booking.expired?

    ActiveRecord::Base.transaction do
      conflicting_bookings_count = Booking.bookings_in_range(booking.start, booking.end).count
      raise ActiveRecord::Rollback if conflicting_bookings_count.positive?

      booking.save!
    end

    if booking.saved_changes?
      render json: { booking_id: booking.id }, status: :created
    else
      render json: { error: 'Conflicting bookings' }, status: :conflict
    end
  rescue ActionController::ParameterMissing => e
    render json: { error: e.message }, status: :unprocessable_entity
  end

  private

  def set_no_cache_headers
    response.headers['Cache-Control'] = 'no-cache, no-store'
    response.headers['Pragma'] = 'no-cache'
    response.headers['Expires'] = 'Mon, 01 Jan 1990 00:00:00 GMT'
  end
end
