# frozen_string_literal: true

require 'test_helper'

class AvailabilityPlannerTest < ActiveSupport::TestCase
  test 'Free day' do
    slots = AvailabilityPlanner.get_slots(15, [], '2015-8-1'.to_time, '2015-8-1'.to_date)
    assert slots.length == 96
  end

  test 'Booked day' do
    booking = Booking.for_day('2015-8-1'.to_date, 0, AvailabilityPlanner::MINS_IN_DAY)
    # booking.start = Time.utc(2015, 8, 1, 0, 0, 0)
    # booking.end = Time.utc(2015, 8, 2, 0, 0, 0)
    slots = AvailabilityPlanner.get_slots(15, [booking], '2015-8-1'.to_time, '2015-8-1'.to_date)
    assert slots.empty?
  end
end
