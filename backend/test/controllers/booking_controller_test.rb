# frozen_string_literal: true

require 'test_helper'

class BookingControllerTest < ActionDispatch::IntegrationTest
  test 'should get availability' do
    get booking_availability_url, params: { day: '2022-01-01', duration: '30' }
    assert_response :success
  end

  test 'should get book' do
    travel_to Time.utc(2022, 0o1, 0o1, 0, 0, 0)

    post booking_book_url, params: { booking_request: { day: '2022-01-01', from: 0, to: 30 } }
    assert_response :success
    assert JSON.parse(response.body)['booking_id'].to_i != 0
    travel_back
  end

  test 'no doulbe booking' do
    travel_to Time.utc(2022, 0o1, 0o1, 0, 0, 0)
    all_count = Booking.all.count
    post booking_book_url, params: { booking_request: { day: '2022-01-01', from: 0, to: 30 } }
    assert_response :success
    assert JSON.parse(response.body)['booking_id'].to_i != 0

    post booking_book_url, params: { booking_request: { day: '2022-01-01', from: 0, to: 15 } }
    assert_response 409

    post booking_book_url, params: { booking_request: { day: '2022-01-01', from: 30, to: 60 } }
    assert_response :success
    assert JSON.parse(response.body)['booking_id'].to_i != 0

    assert all_count + 2 == Booking.all.count
    travel_back
  end

  test 'no possible to book in past' do
    past = Time.now.utc.prev_day
    post booking_book_url, params: { booking_request: { day: past.to_date, from: 0, to: 30 } }
    assert_response :gone
  end
end
