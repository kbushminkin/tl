import './App.css'
import {
  Alert,
  Button,
  Card,
  CardActions,
  CardContent,
  CircularProgress,
  Grid,
  Paper,
  Slider,
  Typography,
} from '@mui/material'
import { DateCalendar, LocalizationProvider } from '@mui/x-date-pickers'
import { AdapterMoment } from '@mui/x-date-pickers/AdapterMoment'
import { useCallback, useEffect, useState } from 'react'
import axios, { AxiosError } from 'axios'
import moment, { Moment } from 'moment'
import { formatDuration } from './formating'
import SlotsPanel from './components/SlotsPanel'
import { bookSlot, getAvailabileSlots } from './api'

function App() {
  const [slots, setSlots] = useState<Array<number>>([])
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [selectedDate, setSelectedDate] = useState<Moment>(moment.utc())
  const [isError, setIsError] = useState<boolean>(false)
  const [duration, setDuration] = useState<number>(30)
  const [selectedSlot, setSelectedSlot] = useState<number | null>(null)
  const [bookedSlot, setBookedSlot] = useState<number | null>(null)
  const [isConflicted, setIsConflicted] = useState<boolean>(false)

  const fetchAvailableSlots = useCallback(async () => {
    try {
      setIsLoading(true)
      const response = await getAvailabileSlots(selectedDate, duration)
      setSlots(response.data)
      setIsError(false)
    } catch (err) {
      setIsError(true)
      setSlots([])
    } finally {
      setSelectedSlot(null)
      setIsLoading(false)
    }
  }, [selectedDate, duration])

  const bookSlotHandler = async () => {
    try {
      setIsLoading(true)
      if (selectedSlot !== null) {
        const response = await bookSlot(
          selectedDate,
          selectedSlot,
          selectedSlot + duration
        )
        setBookedSlot(response.data.booking_id)
        fetchAvailableSlots()
      }
      setIsError(false)
    } catch (err) {
      if (
        axios.isAxiosError(err) &&
        [409, 410].includes((err as AxiosError).response?.status || 0)
      ) {
        setIsConflicted(true)
        await fetchAvailableSlots()
      } else {
        setIsError(true)
      }
      setBookedSlot(null)
    } finally {
      setIsLoading(false)
    }
  }

  useEffect(() => {
    fetchAvailableSlots()
  }, [selectedDate, duration, fetchAvailableSlots])

  useEffect(() => {
    selectedSlot && setIsConflicted(false)
  }, [selectedSlot])

  return (
    <Card sx={{ minWidth: 1000 }}>
      <CardContent>
        <Grid container spacing={2}>
          <Grid item xs={6}>
            <Alert severity="warning">All time is UTC</Alert>
            <Typography gutterBottom>
              <b>1.</b> Pick a day:
            </Typography>
            <LocalizationProvider
              dateAdapter={AdapterMoment}
              dateLibInstance={moment.utc}
            >
              <DateCalendar
                disablePast
                value={selectedDate}
                onChange={(newValue) =>
                  setSelectedDate(newValue ?? moment.utc())
                }
              />
            </LocalizationProvider>
            <Typography gutterBottom>
              Duration: {formatDuration(duration)}
            </Typography>
            <Slider
              aria-label="Duration"
              value={duration}
              onChange={(_ev, newValue) => setDuration(newValue as number)}
              valueLabelDisplay="off"
              step={15}
              marks
              min={15}
              max={1440}
            />
            <Button variant="outlined" onClick={bookSlotHandler}>
              <b>3.</b> Book
            </Button>
            {bookedSlot && (
              <Alert severity="success">
                Booking was successful. Slot id: {bookedSlot}
              </Alert>
            )}
            {isConflicted && (
              <Alert severity="error">
                Selected time frame is unavailible. Please choose another one.
              </Alert>
            )}
            {isError && (
              <Alert severity="error">Failed to fetch data from backend</Alert>
            )}
          </Grid>
          <Grid item xs={3}>
            <Paper>
              {isLoading ? (
                <CircularProgress data-testid="loading" />
              ) : (
                !isError && (
                  <SlotsPanel
                    slots={slots}
                    duration={duration}
                    selectedSlot={selectedSlot}
                    onSelectSlot={setSelectedSlot}
                  />
                )
              )}
            </Paper>
          </Grid>
        </Grid>
      </CardContent>
      <CardActions></CardActions>
    </Card>
  )
}

export default App
