import {
  fireEvent,
  render,
  waitFor,
  waitForElementToBeRemoved,
} from '@testing-library/react'
import axios from 'axios'
import App from './App'
jest.mock('axios')
const mockedAxios = axios as jest.Mocked<typeof axios>

it('Happy path', async () => {
  mockedAxios.get.mockImplementationOnce(() =>
    Promise.resolve({
      data: [0, 30],
    })
  )
  mockedAxios.post.mockImplementationOnce(() =>
    Promise.resolve({
      data: { booking_id: 100 },
    })
  )
  const { getByText, getByTestId } = render(<App />)
  await waitForElementToBeRemoved(() => getByTestId('loading'))
  await waitFor(() => {
    expect(mockedAxios.get).toHaveBeenCalled()
    expect(getByText(/Pick a day/i)).toBeInTheDocument()
    expect(getByText(/Choose suitable slot/i)).toBeInTheDocument()
  })

  fireEvent.click(getByText('00:00-00:30'))
  fireEvent.click(getByText('Book'))

  await waitFor(() => {
    expect(mockedAxios.post).toHaveBeenCalled()
    expect(getByText(/Booking was successful/i)).toBeInTheDocument()
  })
})
