import moment from 'moment'

const zeroPad = (num: number, places: number) =>
  String(num).padStart(places, '0')

export const formatDuration = (duration: number) => {
  const mD = moment.duration(duration, 'minutes')

  return mD.days() === 1
    ? `24:00`
    : `${zeroPad(mD.hours(), 2)}:${zeroPad(mD.minutes(), 2)}`
}

export const getDate = (date: moment.Moment) => date.toISOString().split('T')[0]
