import { Stack, Typography, Chip, Alert } from '@mui/material'
import { formatDuration } from '../formating'

interface ISlotsPanelProps {
  slots: Array<number>
  duration: number
  selectedSlot: number | null
  onSelectSlot: (slot: number) => void
}

function SlotsPanel(props: ISlotsPanelProps) {
  return (
    <Stack spacing={2} p={2}>
      <Typography gutterBottom>
        <b>2.</b> Choose suitable slot:
      </Typography>
      {props.slots.length > 0 ? (
        props.slots.map((slot) => {
          return (
            <Chip
              key={slot}
              variant={slot === props.selectedSlot ? 'filled' : 'outlined'}
              label={`${formatDuration(slot)}-${formatDuration(
                slot + props.duration
              )}`}
              onClick={() => props.onSelectSlot(slot)}
            />
          )
        })
      ) : (
        <Alert severity="warning">No slots available</Alert>
      )}
    </Stack>
  )
}

export default SlotsPanel
