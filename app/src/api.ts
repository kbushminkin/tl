import axios from 'axios'
import { Moment } from 'moment'
import { getDate } from './formating'
const { REACT_APP_API_BASE } = process.env

export async function getAvailabileSlots(
  selectedDate: Moment,
  duration: number
) {
  return axios.get(
    `${REACT_APP_API_BASE}/booking/availability?day=${getDate(
      selectedDate
    )}&duration=${duration}`
  )
}

export async function bookSlot(selectedDate: Moment, from: number, to: number) {
  return axios.post(`${REACT_APP_API_BASE}/booking/book`, {
    booking_request: {
      day: getDate(selectedDate),
      from,
      to,
    },
  })
}
