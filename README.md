# Assumptions 
 - Every time is UTC
 - bookings start and end are aligned to 15 minutes

# How to run
## Backend
```
cd backend/
bundle install
bin/rails db:create db:migrate db:seed
bin/rails s
```

## Frontend
```
cd app
yarn install
yarn start
```
# How to run tests
## Backend
```
cd backend/
bundle install
RAILS_ENV=test bin/rails db:create db:migrate
bin/rails test
```

## Frontend
```
cd app
yarn install
yarn test
```